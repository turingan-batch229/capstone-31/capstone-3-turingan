import {Button, Row, Col} from 'react-bootstrap';
import {Link} from 'react-router-dom';

export default function Banner({data}){

	console.log(data)
	const {title, content, destination, label} = data;

	return(
		<Row>
			<Col className="p-5 text-center">
				<h1>{title}</h1>
				<h3>{content}</h3>
				<Button variant="primary" as={Link} to={destination}>{label}</Button>
			</Col>
		</Row>
		)
}


