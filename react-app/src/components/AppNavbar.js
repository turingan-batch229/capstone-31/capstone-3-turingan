import {useContext} from 'react';
import {Navbar, NavDropdown} from 'react-bootstrap';
import Nav from 'react-bootstrap/Nav';
import { Link, NavLink} from 'react-router-dom';
import UserContext from '../UserContext.js';




export default function AppNavbar(){

	 const {user} = useContext(UserContext);


	return(
		<Navbar id="nav-1" bg="light" expand="lg" className="p-3">
			<Navbar.Brand as = {NavLink} exact to="/">TAC</Navbar.Brand>
			<Navbar.Toggle aria-controls="basic-navbar-homenav"/>
			<Navbar.Collapse id="basic-navbar-nav">
				<Nav className="ms-auto ">
            {
                (user.isAdmin)
                ?
                <Nav.Link as={ NavLink } to="/admin" end>Administrator</Nav.Link>
                :
                <>
                <Nav.Link as={ NavLink } to="/" end>Home</Nav.Link>
                <Nav.Link as={ NavLink } to="/products" end>Products</Nav.Link>
                </>
            }
            {
                (user.id !== null)
                ?	
                    <>
                    
                    <Nav.Link as={ NavLink } to="/user/:userId" end>Profile</Nav.Link>
                    <NavDropdown align="end" id="collasible-nav-dropdown" end>
                    <NavDropdown.Item as={ NavLink } to="/logout" end>Logout</NavDropdown.Item>
                    </NavDropdown>
                    </>
                :
                <>
                    <Nav.Link as={ NavLink } to="/login" end>Login</Nav.Link>
                    <Nav.Link as={ NavLink } to="/register" end>Register</Nav.Link>
                </>
            }
        </Nav>
			</Navbar.Collapse>
		</Navbar>
		)
}
