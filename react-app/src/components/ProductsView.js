import { useState, useEffect, useContext} from 'react';
import { Container, Card, Button, Row, Col, Form } from 'react-bootstrap';
import {useParams, useNavigate, Navigate, Link} from 'react-router-dom';
import UserContext from '../UserContext.js';
import Swal from 'sweetalert2';
	
export default function ProductsView(){

		const navigate = useNavigate();	

	const {user} = useContext(UserContext);

	
	const{productId} = useParams();
	console.log(productId);

	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState("");

	const [quantity, setQuantity] = useState('')
	const [total, setTotal] = useState('')


	useEffect(() => {
		console.log(productId);

		fetch(`${ process.env.REACT_APP_API_URL }/products/${productId}`)
				.then(res => res.json())
				.then(data => {

					console.log(data);

					setName(data.name);
					setDescription(data.description);
					setPrice(data.price);

				});


	},[productId])

	const purchase = (productId) => {
		console.log(productId);
		fetch(`${ process.env.REACT_APP_API_URL }/orders/createOrder`, {
					method: "POST",
					headers: {
						"Content-Type": "application/json",
						Authorization: `Bearer ${ localStorage.getItem('token') }`
					},
					body: JSON.stringify({
						products:{
							productId: productId,
							quantity: quantity
						}

					})
				})
				.then(res => res.json())
				.then(data => {

					console.log(data);

					if(data === true){
						Swal.fire({
							title: "Successfully Purchased!",
							icon: 'success',
							text: `You have successfully ordered ${name}.`
						})

				navigate("/user/:userId");

					}else{
						Swal.fire({
							Title: "Something went wrong!",
							icon: 'error',
							text: "Please try again."
						})
					}

				});
	}

	return(
		(user.token === null)?
			<Navigate to="/login"/>
		:


<Form onSubmit = {(e) => purchase(e)}>
<Container className="mt-5">
	<Row>
		<Col lg={{ span: 4, offset: 4 }}>
			<Card>
				<Card.Body className="text-center">
					<Card.Title>{name}</Card.Title>
					<Card.Subtitle>Product ID:</Card.Subtitle>
					<Card.Text>{productId}</Card.Text>
					<Card.Subtitle>Description:</Card.Subtitle>
					<Card.Text>{description}</Card.Text>
					<Card.Subtitle>Price:</Card.Subtitle>
					<Card.Text>PhP {price}</Card.Text>
					{
						(user.token !== null)?
							(user.isAdmin === false)?
							<>
							<Container fluid="sm">
								<Row>
									<Col>
										<Form.Group className ='mb-1' controllId='quantity'>
											<Form.Label>Quantity: </Form.Label>
											<Form.Control
											type = 'number'
											value={quantity}
											onChange = {e => setQuantity(e.target.value)}
											required/>
										</Form.Group>
									</Col>
									<Col>
										<Form.Group className ='mb-1' controllId='quantity'>
											<Form.Label>Total: </Form.Label>
											<Form.Control
											type = 'number'
											value={quantity*price}
											onChange = {e => setTotal(e.target.value)}
											required
											disabled/>
										</Form.Group>
									</Col>
								</Row>
										<Button variant="primary" onClick={() => purchase(productId)} block>Order Up!</Button>
										<Link className= 'btn btn-success m-3'to='/products' block> Back </Link>
								
							</Container>
							</>
							:
							<>
							<Link className ="btn btn-danger btn-block" to ="/login">Login</Link>
							<Link className= 'btn btn-success m-3'to='/products'> Back </Link>
							</>
						:
						<>
						<Link className ="btn btn-danger btn-block" to ="/login">Login</Link>
						<Link className= 'btn btn-success m-3'to='/products'> Back </Link>
						</>


					}
					
				</Card.Body>		
			</Card>
		</Col>
	</Row>
</Container>
</Form>
	)
}
