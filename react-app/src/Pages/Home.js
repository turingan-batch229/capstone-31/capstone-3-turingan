import Banner from '../components/Banner.js';
import Highligths from '../components/Highlights.js';

const data = {
	title: "Tribus Angeli Cafe",
	content: "Sweet and savory delicacies just a click away.",
	destination: "/products",
	label: "View List"
}

export default function Home() {
	return(
		<>
			<Banner data={data}/>
	      	<Highligths/>
      	</>
		)
}




