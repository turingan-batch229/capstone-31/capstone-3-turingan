import {useEffect, useState} from 'react';
import ProductsCard from '../components/ProductsCard.js';


export default function Products (){

	const [products, setProducts] = useState([]);

		useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/products/available`)
	        .then(res => res.json())
	        .then(data => {
	            
	            console.log(data);

	            setProducts(data.map(product => {
	            	return(
	            		<ProductsCard key={product._id} productProps={product}/>
	            	)

            }))

		});
	}, [])

	return(
		<>
			<h1 className="text-center">Products</h1>
			{products}
		</>

		)

}
